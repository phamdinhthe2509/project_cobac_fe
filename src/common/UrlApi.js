let ipServer = process.env.VUE_APP_IP_SERVER;
let portServer = '8282'
const API_ADD_PLAYER = 'http://' + ipServer + ':' + portServer + '/add-player' ;
const API_PLAYER_TO_OWN = 'http://' + ipServer + ':' + portServer + '/player-to-own';
const API_GET_ALL_PLAYER = 'http://' + ipServer + ':' + portServer + '/get-all-player';
const API_GET_ALL_GAME_TABLE = 'http://' + ipServer + ':' + portServer + '/get-all-game-table';
const API_UPDATE_MONEY_PLAYER = 'http://' + ipServer + ':' + portServer + '/update-money-game-table'

export {
    API_ADD_PLAYER,
    API_GET_ALL_PLAYER,
    API_PLAYER_TO_OWN,
    API_GET_ALL_GAME_TABLE,
    API_UPDATE_MONEY_PLAYER
}