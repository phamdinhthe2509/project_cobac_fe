import { createStore } from 'vuex';

const store = createStore({
    state: {
        sharedData: {
            idOwn : null,
            statusUpdate : null,
            statusAddPlayer : null
        }
    },
    mutations: {
        changeIdOwn(state, newId) {
            state.sharedData.idOwn = newId;
        },
        changeStatusUpdate(state, status) {
            state.sharedData.statusUpdate = status;
        },
        changeStatusAddPlayer(state, status) {
            state.sharedData.statusAddPlayer = status;
        }
    },
    actions: {
        updateIdOwn({ commit }, newId) {
            commit('changeIdOwn', newId);
        },
        updateStatus({ commit }, status) {
            commit('changeStatusUpdate', status);
        },
        updateStatusAddPlayer({ commit }, status) {
            commit('changeStatusAddPlayer', status);
        },
    }
});

export default store;
